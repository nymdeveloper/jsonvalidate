<?php

include '../JsonValidate.php';

class JsonValidatePropertiesCaseTest extends PHPUnit_Framework_TestCase{
    
    
    
    public function testMissingPropertyPhone(){
        
        
        $json = json_decode('{"name": "Adalto","age": 26}');
        
        $fieldsRequired = array("name", "age", "phone");
        
        $arrayResult = Utils\JsonValidate::validateJsonAndShowErrors($json,$fieldsRequired, "Utils\JsonValidate::getMessageError");
        
        $arrayExpected = array("key"=> "phone", "message"=> "Phone is required!");
                
        $this->assertEquals($arrayExpected["key"], $arrayResult[0]["key"]);
        $this->assertEquals($arrayExpected["message"], $arrayResult[0]["message"]);
    
    }
    
    public function testMissingName(){
                
        $json = json_decode('{"age": 26}');
        
        $fieldsRequired = array("name", "age", "phone");
        
        $arrayResult = Utils\JsonValidate::validateJsonAndShowErrors($json,$fieldsRequired, "Utils\JsonValidate::getMessageError");
        
        $arrayExpected = array("key"=> "name", "message"=> "Name is required!");
                
        $this->assertEquals($arrayExpected["key"], $arrayResult[0]["key"]);
        $this->assertEquals($arrayExpected["message"], $arrayResult[0]["message"]);
        
    }
    
    public function testMissingNameAndPhone(){
        
        $json = json_decode('{"age": 26}');
        
        $fieldsRequired = array("name", "age", "phone");
        
        $arrayResult = Utils\JsonValidate::validateJsonAndShowErrors($json,$fieldsRequired, "Utils\JsonValidate::getMessageError");
        
        $arrayExpected = array(
                    array("key"=> "name", "message"=> "Name is required!"),
                    array("key"=> "phone", "message"=> "Phone is required!"),
            );
                
        $this->assertEquals($arrayExpected[0]["key"], $arrayResult[0]["key"]);
        $this->assertEquals($arrayExpected[0]["message"], $arrayResult[0]["message"]);
        $this->assertEquals($arrayExpected[1]["key"], $arrayResult[1]["key"]);
        $this->assertEquals($arrayExpected[1]["message"], $arrayResult[1]["message"]);
        
        
    }
    
    public function testNameIsEmpty(){
        
        $json = json_decode('{"name": "", "age": 26, "phone": 86496525}');
        
        $fieldsRequired = array("name", "age", "phone");
        
        $arrayResult = Utils\JsonValidate::validateJsonAndShowErrors($json,$fieldsRequired, "Utils\JsonValidate::getMessageError");
        
        $arrayExpected = array(
                    array("key"=> "name", "message"=> "Name is required!")
            );
                
        $this->assertEquals($arrayExpected[0]["key"], $arrayResult[0]["key"]);
        $this->assertEquals($arrayExpected[0]["message"], $arrayResult[0]["message"]);
        
    }
    
    public function testJsonIsOk(){
        
        $json = json_decode('{"name": "Adalto", "age": 26, "phone": 86496525}');
        
        $fieldsRequired = array("name", "age", "phone");
        
        $arrayResult = Utils\JsonValidate::validateJsonAndShowErrors($json,$fieldsRequired, "Utils\JsonValidate::getMessageError");
        
        $this->assertCount(0, $arrayResult, "Array Result diferente de 0");
        
    }
    
    
}

?>