<?php

/**
 * Class for validate json
 * @author Adalto Ferreira Evangelista Junior
 */

namespace Utils;

abstract class JsonValidate {

    /**
     * Method for validate fields requireds on json and show message error
     * 
     * @param array $json
     * @param array $fieldsRequired
     * @param function $getMessageFunction
     * @return array 
     */
    public function validateJsonAndShowErrors($json, $fieldsRequired, $getMessageFunction) {

        try {

            $arrayMessageError = array();

            $jsonIsOk = true;

            foreach ($fieldsRequired as $key => $value) {

                if (!isset($json->$value) || $json->$value === "") {

                    if (is_callable($getMessageFunction)) {

                        array_push($arrayMessageError, array("key" => $value,
                            "message" => call_user_func($getMessageFunction, $value)));
                    }
                }
            }

            return $arrayMessageError;
        } catch (Exception $exc) {

            echo $exc->getTraceAsString();
        }
    }
    
    /**
     * Function for get message error.
     *
     * @param type $key
     * @return string 
     */

    public function getMessageError($key) {

        $message = "";


        switch ($key) {

            case "age":
                $message = "Age is required!";
                break;

            case "name":
                $message = "Name is required!";
                break;

            case "phone":
                $message = "Phone is required!";
                break;
        }

        return $message;
    }

}